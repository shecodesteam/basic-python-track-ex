"""
reverse helper function using slicing
"""
def reverse_word_slicing(word):
    return word[::-1]

"""
reverse helper function using reversed() 
"""
def reverse_word_join(word):
    return ''.join(reversed(word)) #The reversed() function returns a reversed iterator object

def reverse_file():
    with open("input.txt", 'r') as in_file, open("reverse.txt", 'w') as out_file:
        for line in in_file:
            word = line.strip('\n') # the line has "\n" at the end we don't need that as part of our word
            out_file.write(reverse_word_join(word) + '\n')

if __name__ == "__main__":
    reverse_file()